

||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
|||                                                                                                        |||
||| ||||||||| |||   ||| ||||||||| |||||||| ||||||||    |||     ||| ||||||||| |||||||| |||     |||     |||  |||
|||    |||    ||||| |||    |||    |||      |||    |||  |||     |||    |||    |||      |||     |||     |||  |||
|||    |||    ||| |||||    |||    |||      |||    |||  |||     |||    |||    |||      |||     |||     |||  |||
|||    |||    |||  ||||    |||    ||||||   |||||||     |||     |||    |||    |||||||| |||     |||     |||  |||
|||    |||    |||   |||    |||    |||      |||  |||     |||   |||     |||    |||       ||     |||     ||   |||
|||    |||    |||   |||    |||    |||      |||   |||     ||| |||      |||    |||        ||    |||    ||    |||
||| ||||||||| |||   |||    |||    |||||||| |||    |||      |||     ||||||||| ||||||||     ||||   ||||      |||
|||                                                                                                        |||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


Waarom wilt u een nieuwe website?
    Huidige website gemaakt door vrijwilliger, bij aanpassingen 
    moet Hannibal zelf aan de slag met HTML en hij wilt iets nieuws
    wat makkelijk aan te passen is en er goed uit ziet op mobile.

Wat wilt u bereiken met de website?
    Informatieve website.

Wat voor uitstraling wilt u opwekken met de website?
    Vrolijk.

Wat is de thema van de website?
    Het is voor een natuurpark.

Welke pagina’s moeten er in de website komen?
    Huidige pagina's uit website behouden.
    Mogelijkheid houden om extra pagina's toe te voegen.

Leveren jullie de content aan die op de website moet komen 
of moeten wij die zelf invullen?
    Zelfde content uit oude website.


Komen er foto's op de website?
    Zelfde foto's gebruiken als de website zonder flash.

Wilt u een blog op de website?
    Nee, geen blog.

Moeten organisaties binnen polderpark Cronesteyn informatie 
op de website kunnen plaatsen?
    Nee, als zij dat willen kunnen ze contact opnemen met ons en beslissen wij of het
    op de website komt of niet.

Wat is de doelgroep die u aan wilt spreken met de website?
    Mensen uit Leiden en omgeving, niet persé een leeftijdscategorie.

Welke kleuren wilt u voor de website?
    Groen, licht en vrolijk.

Moet er een logo worden ontworpen of heeft u er al een?
    Optioneel maar niet nodig.

Welke fonts moeten er gebruikt worden?
    Eigen keuze.

Wilt u een routebeschrijving op de website?
    Ja, het liefste een mapje waar mensen kunnen zien waar ze zich op dat moment bevinden.

Moeten gebruikers contact opnemen via een contactformulier?
    Naam
    E-mail
    Dropdown aan welke vereniging
    Onderwerp
    Bericht

Welke rollen moet de website bevatten? (Denk hierbij aan bezoekers, admins, moderators en auteurs)
    Administrator, beheerder en bezoeker

Wat moeten de rollen kunnen?
    Bezoeker: Bekijken alleen
    Beheerder: Bekijken en inloggen, content beheren en 
    eigen gegevens aanpassen
    admin: beheerder + toevoegen/verwijderen beheerders.

Wat is de deadline van de website?
    Eind Mei